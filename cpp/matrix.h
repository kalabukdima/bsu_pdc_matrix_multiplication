#pragma once

#include <vector>
#include <stdexcept>
#include <type_traits>
#include <sstream>
#include "utils/constants.h"
#include "utils/message.h"


namespace matrix {

struct Shape {
    const int height;
    const int width;

    void assertEqual(const Shape& other) const {
        if constexpr(utils::kDebug) {
            if (operator!=(other)) {
                throw std::runtime_error(utils::Msg()
                    << "Shapes differ: " << *this << " and " << other << "."
                );
            }
        }
    }

    constexpr bool operator==(const Shape& rhs) const noexcept {
        return height == rhs.height && width == rhs.width;
    }

    constexpr bool operator!=(const Shape& rhs) const noexcept {
        return !operator==(rhs);
    }

    friend std::ostream& operator<<(std::ostream& os, const Shape& s) {
        os << "(" << s.height << ", " << s.width << ")";
        return os;
    }
};


template <class T, bool Mutable> class BlockView;

template <class T>
using MutableView = BlockView<T, true>;

template <class T>
using ConstView = BlockView<T, false>;


template <class T>
class Matrix {
public:
    using ValueType = T;

public:
    Matrix(int N, int M, const T& value = T())
        : N(N)
        , M(M)
        , data(N * M, value) {}

    Matrix(int N, int M, std::vector<T> init)
        : N(N)
        , M(M)
        , data(std::move(init))
    {
        if constexpr(utils::kDebug) {
            if (N * M != static_cast<int>(data.size())) {
                throw std::runtime_error(utils::Msg()
                    << "Trying to construct matrix with shape "
                    << shape() << " with " << data.size() << " elements."
                );
            }
        }
    }

    Matrix(const Shape& shape, const T& value = T())
        : N(shape.height)
        , M(shape.width)
        , data(N * M, value) {}


    inline ConstView<T> view(int i, int j, int height, int width) const {
        return view().view(i, j, height, width);
    }

    inline MutableView<T> view(int i, int j, int height, int width) {
        return view().view(i, j, height, width);
    }

    inline ConstView<T> cview(int i, int j, int height, int width) const {
        return view(i, j, height, width);
    }

    inline ConstView<T> view() const noexcept {
        return ConstView<T>(data.data(), N, M, M);
    }

    inline MutableView<T> view() noexcept {
        return MutableView<T>(data.data(), N, M, M);
    }

    inline ConstView<T> cview() const noexcept {
        return view();
    }

    const T& at(int i, int j) const {
        return view().at(i, j);
    }

    T& at(int i, int j) {
        return view().at(i, j);
    }

    Shape shape() const noexcept {
        return {N, M};
    }

private:
    const int N; // height
    const int M; // width
    std::vector<T> data;
};


// Allows access to rectangular block of 2D array where element (i, j) can be
// found at position (i * stride + j) in flat array.
// (0 <= i < height, 0 <= j < width).
// Doesn't own data which it points to.
template <class T, bool Mutable>
class BlockView {
public:
    using ValueType = T;

private:
    using Pointer = std::conditional_t<Mutable, T*, const T*>;
    using Reference = std::conditional_t<Mutable, T&, const T&>;

    friend class Matrix<T>;
    template <class U, bool M> friend class BlockView;

public:
    template <bool RhsIsMutable>
    BlockView(const BlockView<T, RhsIsMutable>& rhs)
        : data(rhs.data)
        , height(rhs.height)
        , width(rhs.width)
        , stride(rhs.stride)
    {
        static_assert(
            RhsIsMutable || !Mutable,
            "Cannot convert from const to mutable"
        );
    }

    Reference at(int i, int j) const {
        if constexpr(utils::kDebug) {
            if (i < 0 || i >= height || j < 0 || j >= width) {
                throw std::out_of_range(utils::Msg()
                    << "Index (" << i << ", " << j << ") is out of bounds of"
                    << " BlockView with shape " << shape() << "."
                );
            }
        }
        return data[i * stride + j];
    }

    BlockView view(int i, int j, int h, int w) const {
        if constexpr(utils::kDebug) {
            if (!(
                i >= 0 && i + h <= height
                && j >= 0 && j + w <= width
            )) {
                throw std::out_of_range(utils::Msg()
                    << "Window (" << i << ", " << j << ", "
                    << h << ", " << w << ") is out of bounds of"
                    << " view with shape " << shape() << "."
                );
            }
        }
        const int shift = i * stride + j;
        return BlockView(data + shift, h, w, stride);
    }

    inline ConstView<T> cview(int i, int j, int h, int w) const {
        return cview().view(i, j, h, w);
    }

    inline BlockView view() const {
        return *this;
    }

    inline ConstView<T> cview() const {
        return *this;
    }

    Shape shape() const noexcept {
        return {height, width};
    }

    bool operator==(ConstView<T> other) const;
    bool operator!=(ConstView<T> other) const;

private:
    BlockView(Pointer data, int height, int width, int stride)
        : data(data)
        , height(height)
        , width(width)
        , stride(stride) {}

private:
    const Pointer data;
    const int height;
    const int width;
    const int stride;
};


template <class T, bool M>
bool BlockView<T, M>::operator==(ConstView<T> other) const {
    shape().assertEqual(other.shape());
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            if (at(i, j) != other.at(i, j)) {
                return false;
            }
        }
    }
    return true;
}

template <class T, bool M>
bool BlockView<T, M>::operator!=(ConstView<T> other) const {
    return !operator==(other);
}

} //namespace matrix
