#pragma once

#include <stdexcept>
#include <type_traits>
#include "constants.h"
#include "message.h"

namespace utils {

template <class T>
T ceilDiv(T n, T k) {
    static_assert(
        std::is_integral_v<T>,
        "`ceilDiv` can only be used with integral types."
    );
    if constexpr(utils::kDebug) {
        if (n < 0 || k <= 0) {
            throw std::runtime_error(utils::Msg()
                << "Cannot divide " << n << " by " << k << "."
            );
        }
    }

    return (n + k - 1) / k;
}

} //namespace utils
