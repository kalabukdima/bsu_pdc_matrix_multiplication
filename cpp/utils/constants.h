#pragma once

namespace utils {

#if defined(DEBUG)
constexpr bool kDebug = true;
#else
constexpr bool kDebug = false;
#endif

} //namespace utils
