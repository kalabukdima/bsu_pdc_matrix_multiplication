#pragma once
#include <sstream>

namespace utils {

class Msg {
public:
    Msg() {}

    explicit Msg(std::string str) {
        stream << str;
    }

    template <class T>
    Msg& operator<<(const T& value) {
        stream << value;
        return *this;
    }

    operator std::string() {
        std::flush(stream);
        return stream.str();
    }

private:
    std::ostringstream stream;
};

} //namespace utils
