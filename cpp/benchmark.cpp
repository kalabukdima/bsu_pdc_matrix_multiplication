#include <thread>
#include <benchmark/benchmark.h>

#include "parallel.h"
#include "tests/common.h"


using matrix::Matrix;

namespace {

void bmMultiplicationWithSharedMatrix(benchmark::State& state) {
    using algorithm::shared_matrix::multiplyInParallel;
    const int n = state.range(0);
    const int r = state.range(1);
    const int t = state.range(2);
    const auto a = tests::generateNumberedMatrix<int>(n, n);
    const auto b = tests::generateNumberedMatrix<int>(n, n);

    for (auto _ : state) {
        benchmark::DoNotOptimize(
            multiplyInParallel(a.view(), b.view(), r, t)
        );
    }
}

void bmMultiplicationWithCopiedMatrix(benchmark::State& state) {
    using algorithm::copied_matrix::multiplyInParallel;
    const int n = state.range(0);
    const int r = state.range(1);
    const int t = state.range(2);
    const auto a = tests::generateNumberedMatrix<int>(n, n);
    const auto b = tests::generateNumberedMatrix<int>(n, n);

    for (auto _ : state) {
        benchmark::DoNotOptimize(
            multiplyInParallel(a.view(), b.view(), r, t)
        );
    }
}

void multiplicationBenchmarkArgs(benchmark::internal::Benchmark* b) {
    for (int threads : {1, 2, 4}) {
        for (int n : {1000, 2000, 3000}) {
            for (int r : {1, 5, 10, 20, 30, 50, 100, 200, 500, n}) {
                if (threads * r <= n) {
                    b->Args({n, r, threads});
                }
            }
        }
    }
    b->Args({2048, 256, 2});
    b->Args({2049, 256, 2});
}

BENCHMARK(bmMultiplicationWithSharedMatrix)
    ->Apply(multiplicationBenchmarkArgs)
    ->Unit(benchmark::kMicrosecond);

BENCHMARK(bmMultiplicationWithCopiedMatrix)
    ->Apply(multiplicationBenchmarkArgs)
    ->Unit(benchmark::kMicrosecond);

} //namespace
