#pragma once

#include <thread>
#include "algorithm.h"

namespace algorithm {

namespace shared_matrix {

template <class T>
class MultiplicationJob {
public:
    using MatrixElementType = T;

public:
    MultiplicationJob(
        ConstView<T> lhs,
        ConstView<T> rhs,
        int blockSize,
        size_t totalWorkers
    ) : lhs(lhs),
        rhs(rhs),
        result(lhs.shape().height, rhs.shape().width),
        blockSize(blockSize),
        totalWorkers(totalWorkers),
        blockRowsPerWorker(calculateBlockRowsPerWorker(
            utils::ceilDiv(lhs.shape().height, blockSize),
            totalWorkers
        )) {}

    static int calculateBlockRowsPerWorker(int rows, int workers) {
        if (rows < workers) {
            throw std::runtime_error(utils::Msg()
                << "Can not split " << rows << " block rows between "
                << workers << " threads."
            );
        }
        return rows / workers;
    }

    auto createWorker(size_t workerIndex) {
        return [this, workerIndex] () {
            const int firstRow = workerIndex * blockRowsPerWorker * blockSize;
            const int rows = static_cast<int>(workerIndex) + 1 == totalWorkers
                ? result.shape().height - firstRow
                : blockRowsPerWorker * blockSize;

            const auto lhsChunk = lhs.view(
                firstRow, 0,
                rows, lhs.shape().width
            );
            const auto rhsChunk = rhs.view();
            const auto dstChunk = result.view(
                firstRow, 0,
                rows, result.shape().width
            );

            algorithm::multiplyBlockwise(
                lhsChunk, rhsChunk, dstChunk,
                blockSize
            );
        };
    }

    // Call only after all workers have finished.
    Matrix<T> getResult() {
        return std::move(result);
    }

private:
    const ConstView<T> lhs;
    const ConstView<T> rhs;
    Matrix<T> result;
    const int blockSize;
    const int totalWorkers;
    const int blockRowsPerWorker;
};


template <class T>
Matrix<T> multiplyInParallel(
    ConstView<T> lhs,
    ConstView<T> rhs,
    int blockSize,
    size_t numOfThreads
) {
    MultiplicationJob<T> job(
        lhs, rhs,
        blockSize,
        numOfThreads
    );

    std::vector<std::thread> threads;
    for (size_t threadId = 0; threadId < numOfThreads; ++threadId) {
        threads.emplace_back(job.createWorker(threadId));
    }

    for (auto& thread : threads) {
        thread.join();
    }

    return job.getResult();
}

} //namespace shared_matrix


// Sorry for copypasting

namespace copied_matrix {

template <class T>
class MultiplicationJob {
public:
    using MatrixElementType = T;

public:
    MultiplicationJob(
        ConstView<T> lhs,
        ConstView<T> rhs,
        int blockSize,
        size_t totalWorkers
    ) : lhs(lhs),
        rhs(rhs),
        result(lhs.shape().height, rhs.shape().width),
        blockSize(blockSize),
        totalWorkers(totalWorkers),
        blockRowsPerWorker(calculateBlockRowsPerWorker(
            utils::ceilDiv(lhs.shape().height, blockSize),
            totalWorkers
        )) {}

    static int calculateBlockRowsPerWorker(int rows, int workers) {
        if (rows < workers) {
            throw std::runtime_error(utils::Msg()
                << "Can not split " << rows << " block rows between "
                << workers << " threads."
            );
        }
        return rows / workers;
    }

    auto createWorker(size_t workerIndex) {
        return [this, workerIndex] () {
            const int firstRow = workerIndex * blockRowsPerWorker * blockSize;
            const int rows = static_cast<int>(workerIndex) + 1 == totalWorkers
                ? result.shape().height - firstRow
                : blockRowsPerWorker * blockSize;

            const auto lhsChunkView = lhs.view(
                firstRow, 0,
                rows, lhs.shape().width
            );
            const auto rhsChunkView = rhs.view();
            const auto dstChunkView = result.view(
                firstRow, 0,
                rows, result.shape().width
            );

            Matrix<T> lhsChunkCopy(lhsChunkView.shape());
            algorithm::copy(lhsChunkView, lhsChunkCopy.view());
            Matrix<T> rhsChunkCopy(rhsChunkView.shape());
            algorithm::copy(rhsChunkView, rhsChunkCopy.view());
            Matrix<T> dstChunk(dstChunkView.shape());

            algorithm::multiplyBlockwise(
                lhsChunkCopy.cview(), rhsChunkCopy.cview(), dstChunk.view(),
                blockSize
            );
            algorithm::copy(dstChunk.cview(), dstChunkView);
        };
    }

    // Call only after all workers have finished.
    Matrix<T> getResult() {
        return std::move(result);
    }

private:
    const ConstView<T> lhs;
    const ConstView<T> rhs;
    Matrix<T> result;
    const int blockSize;
    const int totalWorkers;
    const int blockRowsPerWorker;
};


template <class T>
Matrix<T> multiplyInParallel(
    ConstView<T> lhs,
    ConstView<T> rhs,
    int blockSize,
    size_t numOfThreads
) {
    MultiplicationJob<T> job(
        lhs, rhs,
        blockSize,
        numOfThreads
    );

    std::vector<std::thread> threads;
    for (size_t threadId = 0; threadId < numOfThreads; ++threadId) {
        threads.emplace_back(job.createWorker(threadId));
    }

    for (auto& thread : threads) {
        thread.join();
    }

    return job.getResult();
}

} //namespace copied_matrix

} //namespace algorithm
