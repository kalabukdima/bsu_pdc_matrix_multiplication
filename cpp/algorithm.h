#pragma once

#include "utils/constants.h"
#include "utils/message.h"
#include "utils/math.h"
#include "matrix.h"

namespace algorithm {

using matrix::Matrix;
using matrix::ConstView;
using matrix::MutableView;

template <class T>
void copy(ConstView<T> src, MutableView<T> dst) {
    src.shape().assertEqual(dst.shape());
    const auto shape = src.shape();
    for (int i = 0; i < shape.height; ++i) {
        for (int j = 0; j < shape.width; ++j) {
            dst.at(i, j) = src.at(i, j);
        }
    }
}

template <class T>
void add(T& lhs, const T& rhs) {
    lhs += rhs;
}

template <class T>
void add(MutableView<T> lhs, ConstView<T> rhs) {
    lhs.shape().assertEqual(rhs.shape());
    const auto shape = lhs.shape();
    for (int i = 0; i < shape.height; ++i) {
        for (int j = 0; j < shape.width; ++j) {
            add(lhs.at(i, j), rhs.at(i, j));
        }
    }
}


namespace internal {

inline void checkSutabilityForMultiplication(
    matrix::Shape lhs,
    matrix::Shape rhs,
    matrix::Shape dst
) {
    if (
        lhs.width != rhs.height
        || lhs.height != dst.height
        || rhs.width != dst.width
    ) {
        throw std::runtime_error(utils::Msg()
            << "Can not multiply matrices with shapes " << lhs
            << " and " << rhs << " to get matrix with shape "
            << dst << "."
        );
    }
}

inline void checkSutabilityForMultiplication(
    matrix::Shape lhs,
    matrix::Shape rhs
) {
    if (lhs.width != rhs.height) {
        throw std::runtime_error(utils::Msg()
            << "Can not multiply matrices with shapes " << lhs
            << " and " << rhs << "."
        );
    }
}


template <class T>
void addProduct(const T& lhs, const T& rhs, T& dst) {
    dst += lhs * rhs;
}

// Multiplies `lhs` and `rhs` and adds result to `dst`.
// This function can be used with `ConstView<ConstView<ArithmeticType>>`.
template <class L, class R, class DestView>
void addProduct(
    ConstView<L> lhs,
    ConstView<R> rhs,
    DestView dst
) {
    if constexpr(utils::kDebug) {
        checkSutabilityForMultiplication(lhs.shape(), rhs.shape(), dst.shape());
    }

    const int n = lhs.shape().height;
    const int k = lhs.shape().width;
    const int m = rhs.shape().width;

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            for (int z = 0; z < k; ++z) {
                addProduct(lhs.at(i, z), rhs.at(z, j), dst.at(i, j));
            }
        }
    }
}

} //namespace internal


template <class T>
Matrix<T> multiplySimple(ConstView<T> lhs, ConstView<T> rhs) {
    static_assert(
        std::is_arithmetic_v<T>,
        "`multiplySimple` can only be used with arithmetic types"
    );
    internal::checkSutabilityForMultiplication(lhs.shape(), rhs.shape());

    Matrix<T> result(lhs.shape().height, rhs.shape().width);
    internal::addProduct(lhs, rhs, result.view());
    return result;
}

template <class T>
Matrix<T> multiplySimple(
    const Matrix<T>& lhs,
    const Matrix<T>& rhs
) {
    return multiplySimple(lhs.cview(), rhs.cview());
}

template <class T>
void multiplyBlockwise(
    ConstView<T> lhs,
    ConstView<T> rhs,
    MutableView<T> result,
    int blockSize
) {
    static_assert(
        std::is_arithmetic_v<T>,
        "`multiplyBlockwise` can only be used with arithmetic types"
    );
    internal::checkSutabilityForMultiplication(
        lhs.shape(),
        rhs.shape(),
        result.shape()
    );

    const int n = lhs.shape().height;
    const int k = lhs.shape().width;
    const int m = rhs.shape().width;

    const auto ithChunk = [blockSize] (int i, int max) {
        const int begin = i * blockSize;
        const int size = std::min(max, (i + 1) * blockSize) - begin;
        return std::make_pair(begin, size);
    };

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            for (int z = 0; z < k; ++z) {
                const auto [iBegin, iSize] = ithChunk(i, n);
                const auto [jBegin, jSize] = ithChunk(j, m);
                const auto [zBegin, zSize] = ithChunk(z, k);
                internal::addProduct(
                    lhs.view(iBegin, zBegin, iSize, zSize),
                    rhs.view(zBegin, jBegin, zSize, jSize),
                    result.view(iBegin, jBegin, iSize, jSize)
                );
            }
        }
    }
}

template <class T>
Matrix<T> multiplyBlockwise(
    const Matrix<T>& lhs,
    const Matrix<T>& rhs,
    int blockSize
) {
    Matrix<T> result(lhs.shape().height, rhs.shape().width);
    multiplyBlockwise(lhs.view(), rhs.view(), result.view(), blockSize);
    return result;
}

} //namespace algorithm
