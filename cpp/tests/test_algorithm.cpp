#include <gtest/gtest.h>
#include "common.h"
#include "../algorithm.h"
#include "../parallel.h"

using matrix::Matrix;
using tests::generateNumberedMatrix;

TEST(Algorithm, Copy) {
    Matrix<int> m(3, 2);
    m.at(1, 1) = 1;
    m.at(2, 1) = 2;
    algorithm::copy(m.cview(1, 1, 2, 1), m.view(0, 0, 2, 1));
    ASSERT_EQ(m.at(0, 0), 1);
    ASSERT_EQ(m.at(0, 1), 0);
    ASSERT_EQ(m.at(1, 0), 2);
    ASSERT_EQ(m.at(1, 1), 1);
    ASSERT_EQ(m.at(2, 0), 0);
    ASSERT_EQ(m.at(2, 1), 2);

    Matrix<int> other(2, 3);
    algorithm::copy(m.cview(1, 0, 2, 2), other.view(0, 0, 2, 2));
    ASSERT_TRUE(m.view(1, 0, 2, 2) == other.view(0, 0, 2, 2));
    ASSERT_EQ(other.at(0, 2), 0);
    ASSERT_EQ(other.at(1, 2), 0);
}

TEST(Algorithm, Add) {
    Matrix<int> m(3, 2, 0);
    m.at(0, 1) = 1;
    m.at(1, 1) = 2;
    algorithm::add(m.at(2, 1), 3);

    algorithm::add(m.view(0, 0, 3, 1), m.cview(0, 1, 3, 1));
    algorithm::add(m.view(0, 0, 3, 1), m.cview(0, 1, 3, 1));

    ASSERT_EQ(m.at(0, 0), 2);
    ASSERT_EQ(m.at(0, 1), 1);
    ASSERT_EQ(m.at(1, 0), 4);
    ASSERT_EQ(m.at(1, 1), 2);
    ASSERT_EQ(m.at(2, 0), 6);
    ASSERT_EQ(m.at(2, 1), 3);
}

TEST(Algorithm, MultiplySimple) {
    using algorithm::multiplySimple;

    Matrix<int> a(2, 3);
    a.at(0, 0) = 1;
    a.at(0, 1) = 3;
    a.at(0, 2) = -1;
    a.at(1, 0) = 2;
    a.at(1, 1) = 0;
    a.at(1, 2) = 1;
    Matrix<int> b(3, 2);
    b.at(0, 0) = 4;
    b.at(0, 1) = 0;
    b.at(1, 0) = 0;
    b.at(1, 1) = -1;
    b.at(2, 0) = -2;
    b.at(2, 1) = 1;

    const Matrix<int> c = multiplySimple(a, b);
    ASSERT_EQ(c.shape(), matrix::Shape({2, 2}));
    ASSERT_EQ(c.at(0, 0), 6);
    ASSERT_EQ(c.at(0, 1), -4);
    ASSERT_EQ(c.at(1, 0), 6);
    ASSERT_EQ(c.at(1, 1), 1);

    const Matrix<int> c1 = multiplySimple(a.cview(), b.cview());
    ASSERT_EQ(c.view(), c1.view());

    ASSERT_EQ(
        multiplySimple(c, Matrix<int>(2, 100500)).cview(),
        Matrix<int>(2, 100500).cview()
    );
}

TEST(Algorithm, MultiplyBlockwiseSimple) {
    using algorithm::multiplyBlockwise;

    Matrix<int> a(2, 3);
    a.at(0, 0) = 1;
    a.at(0, 1) = 3;
    a.at(0, 2) = -1;
    a.at(1, 0) = 2;
    a.at(1, 1) = 0;
    a.at(1, 2) = 1;
    Matrix<int> b(3, 2);
    b.at(0, 0) = 4;
    b.at(0, 1) = 0;
    b.at(1, 0) = 0;
    b.at(1, 1) = -1;
    b.at(2, 0) = -2;
    b.at(2, 1) = 1;

    const Matrix<int> c1 = multiplyBlockwise(a, b, 1);
    const Matrix<int> c2 = multiplyBlockwise(a, b, 2);
    const Matrix<int> c3 = multiplyBlockwise(a, b, 3);
    const Matrix<int> c4 = multiplyBlockwise(a, b, 4);

    ASSERT_EQ(c1.shape(), matrix::Shape({2, 2}));
    ASSERT_EQ(c1.at(0, 0), 6);
    ASSERT_EQ(c1.at(0, 1), -4);
    ASSERT_EQ(c1.at(1, 0), 6);
    ASSERT_EQ(c1.at(1, 1), 1);

    ASSERT_EQ(c1.cview(), c2.cview());
    ASSERT_EQ(c2.cview(), c3.cview());
    ASSERT_EQ(c3.cview(), c4.cview());
}

TEST(Algorithm, MultiplyBlockwiseLong) {
    using algorithm::multiplyBlockwise;

    ASSERT_EQ(
        multiplyBlockwise(
            generateNumberedMatrix<int>(3, 2),
            Matrix<int>(2, 1000500),
            2
        ).cview(),
        Matrix<int>(3, 1000500).cview()
    );
}

TEST(Algorithm, MultiplyBlockwiseAdvanced) {
    using algorithm::multiplySimple;
    using algorithm::multiplyBlockwise;
    constexpr int N = 12;
    constexpr int M = 12;
    constexpr int K = 32;

    for (int n = 1; n < N; ++n) {
        for (int m = 1; m < M; ++m) {
            for (int k = 0; k < K; ++k) {
                const auto a = generateNumberedMatrix<int>(n, k);
                const auto b = generateNumberedMatrix<int>(k, m);
                const auto expected = multiplySimple(a, b);
                for (
                    int blockSize = 1;
                    blockSize <= std::max(k, std::max(n, m)) + 1;
                    ++blockSize
                ) {
                    const auto result = multiplyBlockwise(a, b, blockSize);
                    ASSERT_EQ(result.view(), expected.view());
                }
            }
        }
    }
}

TEST(Parallel, Multiplying) {
    namespace shared = algorithm::shared_matrix;
    namespace copied = algorithm::copied_matrix;
    constexpr int N = 12;
    constexpr int M = 12;
    constexpr int K = 24;
    constexpr int T = 4;

    int successes = 0;
    for (int n = 1; n < N; ++n) {
        for (int m = 1; m < M; ++m) {
            for (int k = 0; k < K; ++k) {
                const auto a = generateNumberedMatrix<int>(n, k);
                const auto b = generateNumberedMatrix<int>(k, m);
                const auto expected = algorithm::multiplySimple(a, b);
                for (
                    int blockSize = 1;
                    blockSize <= std::max(k, std::max(n, m));
                    ++blockSize
                ) {
                    for (int threads = 2; threads <= T; ++threads) {
                        try {
                            const auto result0 = shared::multiplyInParallel(
                                a.view(), b.view(),
                                blockSize, threads
                            );
                            ASSERT_EQ(result0.view(), expected.view());

                            const auto result1 = copied::multiplyInParallel(
                                a.view(), b.view(),
                                blockSize, threads
                            );
                            ASSERT_EQ(result1.view(), expected.view());

                            ++successes;
                        } catch (const std::runtime_error&) {
                            // Block seems to be too big.
                        }
                    }
                }
            }
        }
    }
    ASSERT_GT(successes, 16000);
}
