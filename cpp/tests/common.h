#pragma once

#include <vector>
#include <numeric>
#include "../matrix.h"

namespace tests {

template <class T>
matrix::Matrix<T> generateNumberedMatrix(int n, int m, T firstValue = 0) {
    std::vector<T> v(n * m);
    std::iota(v.begin(), v.end(), firstValue);
    return matrix::Matrix<T>(n, m, v);
}

} //namespace tests
