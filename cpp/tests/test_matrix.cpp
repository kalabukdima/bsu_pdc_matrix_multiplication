#include <gtest/gtest.h>
#include "../matrix.h"
#include "../utils/message.h"

#ifdef DEBUG
#define ASSERT_THROW_IN_DEBUG_ONLY(A, E) ASSERT_THROW(A, E)
#else
#define ASSERT_THROW_IN_DEBUG_ONLY(A, E) ASSERT_NO_THROW(A)
#endif

using matrix::Matrix;

TEST(Matrix, CopyingViews) {
    using ConstView = matrix::ConstView<int>;
    using MutableView = matrix::MutableView<int>;

    Matrix<int> a(2, 2, 0);
    MutableView mutableView = a.view();
    ConstView constView = mutableView;
    ConstView anotherConstView = constView;
    mutableView.at(0, 0) = 1;
    mutableView.at(1, 0) = 3;
    a.view().at(0, 1) = 2;
    a.view().at(1, 1) = 4;

    ASSERT_EQ(anotherConstView.at(0, 0), 1);
    ASSERT_EQ(anotherConstView.at(0, 1), 2);
    ASSERT_EQ(anotherConstView.at(1, 0), 3);
    ASSERT_EQ(anotherConstView.at(1, 1), 4);
}

TEST(Matrix, CreatingViews) {
    const Matrix<int> a(3, 2, 0);
    ASSERT_THROW_IN_DEBUG_ONLY(
        a.view(1, 0, 3, 2),
        std::out_of_range
    );
    ASSERT_THROW_IN_DEBUG_ONLY(
        a.view(2, 0, 1, 3),
        std::out_of_range
    );
    ASSERT_THROW_IN_DEBUG_ONLY(
        a.view(0, -1, 1, 1),
        std::out_of_range
    );
    ASSERT_THROW_IN_DEBUG_ONLY(
        a.view(-1, 0, 1, 1),
        std::out_of_range
    );
    ASSERT_NO_THROW(
        a.view(3, 0, 0, 2)
    );
    ASSERT_THROW_IN_DEBUG_ONLY(
        a.view(3, 0, 0, 2).at(0, 0),
        std::out_of_range
    );
}

TEST(Matrix, Construction) {
    const Matrix<int> m(2, 3, {1, 2, 3, 4, 5, 6});
    ASSERT_EQ(m.at(0, 0), 1);
    ASSERT_EQ(m.at(0, 1), 2);
    ASSERT_EQ(m.at(0, 2), 3);
    ASSERT_EQ(m.at(1, 0), 4);
    ASSERT_EQ(m.at(1, 1), 5);
    ASSERT_EQ(m.at(1, 2), 6);
}

TEST(Matrix, ElementAccess) {
    constexpr int N = 3;
    constexpr int M = 4;

    Matrix<int> m(N, M);
    {
        const auto v = m.view(1, 1, 2, 3);
        v.at(0, 0) = 6;
        v.at(0, 1) = 7;
        v.at(0, 2) = 8;
        v.at(1, 0) = 10;
        v.at(1, 1) = 11;
        v.at(1, 2) = 12;
        ASSERT_THROW_IN_DEBUG_ONLY(v.at(0, -1), std::out_of_range);
        ASSERT_THROW_IN_DEBUG_ONLY(v.at(0, 3), std::out_of_range);
        ASSERT_THROW_IN_DEBUG_ONLY(v.at(2, 2), std::out_of_range);
    }
    {
        const auto v = m.view(0, 0, 3, 2);
        ASSERT_EQ(v.at(1, 1), 6);
        ASSERT_EQ(v.at(2, 1), 10);
        v.at(0, 0) = 1;
        v.at(0, 1) = 2;
        v.at(1, 0) = 5;
        v.at(2, 0) = 9;
        ASSERT_THROW_IN_DEBUG_ONLY(v.at(1, 2), std::out_of_range);
    }
    {
        const auto v = m.view(0, 1, 1, 3);
        ASSERT_EQ(v.at(0, 0), 2);
        ASSERT_EQ(v.at(0, 1), 0);
        ASSERT_EQ(v.at(0, 2), 0);
        v.at(0, 1) = 3;
        v.at(0, 2) = 4;
    }
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < M; ++j) {
            ASSERT_EQ(m.view().at(i, j), i * M + j + 1);
        }
    }
}

TEST(Matrix, Comparison) {
    Matrix<int> m(2, 3);
    m.at(0, 1) = 1;
    m.at(1, 0) = 1;
    ASSERT_TRUE(m.view(0, 0, 2, 2) == m.view(0, 0, 2, 2));
    ASSERT_TRUE(m.view(0, 1, 1, 2) == m.view(1, 0, 1, 2));
    ASSERT_FALSE(m.view(0, 0, 2, 1) == m.view(0, 2, 2, 1));
    ASSERT_THROW_IN_DEBUG_ONLY(
        m.view(0, 0, 1, 1) == m.view(0, 0, 2, 1),
        std::runtime_error
    );
}
