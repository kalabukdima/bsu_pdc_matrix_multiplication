import json
import matplotlib.pyplot as plt


NS = [1000, 2000, 3000]
TS = [1, 2, 4]

j = json.loads(open('../results.json', 'r').read())

time = {}
for n in NS:
    time[n] = {}
    for t in TS:
        time[n][t] = {}

for measurment in j['benchmarks']:
    # TODO Handle benchmarks with different names. Current assumption is that
    # file contains results for single benchmark type (bmBlockwiseMultiplication).
    _, n, r, t = measurment['name'].split('/')
    n, r, t = map(int, (n, r, t))
    print(n, r, t)
    time[n][t][r] = measurment['real_time']

for n in NS:
    plt.figure(figsize=(16, 8))
    for t in TS:
        rs = []
        ts = []
        for r, result in time[n][t].items():
            rs.append(r)
            ts.append(result)
        plt.plot(rs, ts)
    plt.yscale('log')
    plt.title('N={}'.format(n))
    plt.legend(['T={}'.format(t) for t in TS])
    plt.show()
